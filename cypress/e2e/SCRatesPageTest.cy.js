describe('Rates page test', () => {
  beforeEach(()=>{
    cy.visit('https://spectrocoin.com/en/bitcoin-price-rates.html');
  });

  it('Refference currency (USD): if past 24h rate for 1st currency is positive', () => {
    cy.get('#currency-select').click();
		cy.get('#react-select-currency-select-option-0').should('have.text', 'EUR');
		cy.get('#react-select-currency-select-option-1').should('have.text', 'GBP');
		cy.get('#react-select-currency-select-option-2').should('have.text', 'USD');
		cy.get('#react-select-currency-select-option-2').click();
    cy.get('#currency-select').contains('USD');
    cy.get('[data-cy=rates] > tbody').find('tr').eq(0).find('td').eq(2).should('include.text', '+');
  })
})